const log = console.log
/*****************************************
 * If / Else Statemenets
 */

//  var firstName = 'John';
// //  var civilStatus = 'single';
//  var civilStatus = 'single';

//  if(civilStatus === 'married'){
//      log(firstName + ' is married')
//  } else {
//      log(firstName + ' will hopefully marry soon :)')
//  }

//  var massMark   = 90; //kg
//  var heightMark = 1.69; //meters

//  var massJohn   = 92;
//  var heightJohn = 1.95

//  var BMIMark = massMark / (heightMark * heightMark)
//  var BMIJohn = massJohn / (heightJohn * heightJohn)

// if(BMIMark > BMIJohn){
//     console.log('Mark\'s is higher than John\'s ')
// } else{
//     console.log('John\'s is higher than Mark\'s ')
// }

/**************************
 * BOOLEAN LOGIC
 */

// var firstName = 'John';
// var age = 21

// if(age < 13) {
//     console.log(firstName + ' is a boy.')
// } else if(age >= 13 && age <= 20){
//     console.log(firstName + ' is a teenager')
// } else if( age > 20 && age < 30) {
//    console.log(firstName + ' is a young man')
// } else {
//     console.log(firstName + ' is an old man')
// }

/*************************************************
 * The Teranary Operator and Switch Statements
 */

//  var firstName = 'John';
//  var age = 16;

//  age >= 18 ? console.log(firstName + ' drinks beer') : console.log(firstName + ' drinks Juice')

//  //way 1
//  var drink = age >= 18 ? 'beer' : 'juice'
// console.log('drink', drink)

// //way 2
// if( age >= 18){
//     var drink = 'beer'
//     console.log(drink)
// } else {
//     var drink = 'juice'
//     console.log(drink)
// }



/****************************************
 * SWITCH STATEMENT
 */

// var age = 10;
// var firstName = 'john'

//  var job = 'teacher';
//  switch(job){
//      case 'teacher':
//          console.log(firstName + ' teacheds kids how to code')
//          break;
//     case 'driver': 
//          console.log(firstName + ' drives an uber in lisbon')
//          break;
//     case 'designer':
//         console.log(firstName + ' designs beautiful websites')
//         break;
//     default : 
//         console.log(firstName + ' does something else')
//  }

// switch(true) {
//     case age < 13 :
//         console.log(firstName + ' is a boy');
//         break;
//     case age >= 13 && age < 20:
//         console.log(firstName + ' is a teenager');
//         break;
//     case age >20 && age < 30:
//         console.log(firstName + ' is a young man');
//         break;
//     default : 
//         console.log(firstName + 'is a man')
// }


/***************************************************
 * TRUTHY AND FALSY VALUES AND EQUALITY OPERATORS
 */

// falsy values: undefined, null, 0, '', NaN
// Truthy values : Not falsy values

//example
var height; //its just declared not define the value

// height = 12 //true
// height = 0 //falsy
// height = 'twnty' //true
// height = '' //false

height = 23

if(height || height === 0) {
    console.log('Variable is defined');
} else {
    console.log('Variable has NOT been defined')
}

// Equality Operators
if(height == '23'){
    console.log('the == operator does type coercion!')
}