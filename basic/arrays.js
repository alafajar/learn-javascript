/******************
 * ARRAYS
 */

// index from 0, index urutan data di array
// elements from 1, element itu isi


//mutate = dapat diubah, immutate = tidak dapat diubah datanya

// initialize new Array
var names = ['john', 'mark', 'jane']
var years = new Array(1990,1969, 1948)

console.log(names[2]);
console.log(names.length);

//Mutate array data
names[1] = 'Ben';
names[names.length] = 'Mary';
// names[5] = 'Mary'

console.log('names', names)


// Different data types
var john = ['john', 'smith', 1990, 'teacher', false, 'designer']

john.push('blue'); //added 1 element of array
john.unshift('Mr.');
console.log(john)

console.log(john.indexOf(23))
console.log(john.indexOf('blue'))

var isDesigner = john.indexOf('designer') === -1 ? 'John is NOT a designer' : 'John IS a designer'
console.log(isDesigner)