const log = console.log
/********************************* 
 * 1. BASIC: VARIABLES AND DATA TYPES
 */
// var firstName = 'John';
// log(firstName)

// var lastName  = 'Smith';
// var age       = 28;

// var fullAge = true;
// log(fullAge)

// var job;
// log(job) //undefined = non existent
// job = 'teacher'
// log(job)


/*************************************
 * 2. VARIABLE MUTATION AND TYPE COERCION
 */ 

//  undefined condition: it might be variable are not declared or there's no a value in variable declare

//  var firstName = 'John';
//  var age = 28

//  //Type Coercion
// log(firstName + ' ' + age)

// var job, isMarried;
// job       = 'teacher';
// isMarried = false;

// log(firstName + ' is a ' + age + ' year old' + job + '. Is he married?' + isMarried)

// //Variable Mutation
// age = 'twenty eight';
// job = 'driver';

// alert(firstName + 'is a ' + age + ' year old ' + job + '. Is he married? ' + isMarried )

// var lastName = prompt('WHat is his last Name?')
// log(firstName + '' + lastName)

/*********************
 * 3. Basic Operators
 */
var year, yearJohn, yearMark
now = 2018;
ageJohn = 28;
ageMark = 33;

yearJohn = now - ageJohn;
yearMark = now - ageMark;
log('yearJohn', yearJohn)

//Math Operator
log('now', now + 2)
log('now', now * 2)
log('now', now / 10)

//Logical Operator
// var johnOlder = ageJohn < ageMark;
// log('john Older ', johnOlder)

//its posible give a variable without declaring a value BUT should call it inside declared variable
// nasi = 'nasi '
// tempe = 'tempe '

// var nasiRames = 'ayam ' + nasi + tempe
// log('nasiRames', nasiRames)

//Type of operator
// log(typeof johnOlder);
// log(typeof ageJohn);
// log(typeof 'Mark is older than John');
// var x;
// log(typeof x);

/********************************************
 * 4. OPERATOR PRECEDENCE (Hak lebih tinggi)
 */

var now      = 2018;
var yearJohn = 1989;
var fullAge  = 18;

// Multiple Operators
var isFullAge = now - yearJohn >= fullAge; //true
log('isFullAge', isFullAge)

// Grouping
var ageJohn = now - yearJohn;
var ageMark = 35;
var average = ageJohn + ageMark / 2
log('average', average)
average = (ageJohn + ageMark) / 2
log('average', average)

// Multiple Assignments
var x, y;
//precedence table assignment = right to left
x = y = (3 + 5) * 4 - 6; // 8 * 4 - 6 // 32 - 6 // 26
log('x assign', x)

//More operators
x = x*2;
// x *= 2; same
log('x operator', x)

//Increment
x-- // x++
log('x increment', x)