const log = console.log
/**************************
 * CODING CHALLENGE 1
 */

 /**
  * Mark and john are trying to compare their BMI (Body Mass Index) , which is calculated using the formula: BMI = mass / height ^2 = mass / (height*height).
  * (mass in kg and height in meter).
  * 
  * 1. Store Marks and john's mass and height in variables
  * 2. calculate both their BMIs
  * 3. Create a boolean variable containing information about whether Mark has a higher BMI thank John.
  * 4. Print a string to the console containing the variable from steop 3. (Something like "is Mark's BMI higher than John's? true")
  */

// hipotesis
//   var mark, john;
//   var bmi = mass;
//   var height2 = mass/(height * height)

//   var result = mark > john

//   log('Is Mark\'s BMI higher than John\'s? true', result)


  // Answer
  var massMark   = 78; //kg
  var heightMark = 1.69; //meters

  var massJohn   = 92;
  var heightJohn = 1.95

  var BMIMark = massMark / (heightMark * heightMark)
  var BMIJohn = massJohn / (heightJohn * heightJohn)

  var markHigherBMI = BMIMark > BMIJohn
  log('Is Mark\'s BMI higher than John\'s? ', markHigherBMI)