/*******************************
 * OBJECT AND PROPERTIES
 */

 //Object Literal
//  var john = {
//      firstName : 'John',
//      lastName  : 'Smith',
//      birthYear : 1990,
//      family    : ['jane', 'mark', 'bob', 'emily'],
//      job       : 'teacher',
//      isMarried : false
//  };

//  console.log(john)
// console.log(john['lastName'])
// var x = 'job'
// console.log(john[x])

// john.job = 'designer';
// john['isMarried'] = true
// console.log(john)

// // another new object syntax
// var jane          = new Object();
// jane.name         = 'Jane';
// jane.birthYear    = 1969;
// jane[lasttName]   = 'smith' //error
// jane['lasttName'] = 'smith'
// console.log(jane)


//Object and methods

var john = {
    firstName : 'John',
    lastName: 'Smith',
    birthYear: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    // functional usually
    // calcAge : function(birthYear) {
    //     return 2018 - birthYear
    // },

    // functional using this in object
    calcAge : function() {
        // return 2018 - this.birthYear
        //we can also create object using this
        // create object age
        this.age =  2018 - this.birthYear
    }
}

// THIS in object
// this what it means here in this context is john
// this means this object, the present, the current object
john.calcAge()
console.log(john)
