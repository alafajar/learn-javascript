/**********************
 * CODING CHALLENGE 4
 */

 /**
  * Let's remember the first coding challenger where Mark and John compared their BMIs. Let's now implement the same functionality with objects and methods.
  * 1. For Each of them, create an object with properties for their full name, mass, and height
  * 2. then, add a method to each object to calculate the BMI. Save the BMI to the object and alaso return it from the method
  * 3. In the end, log to the console who has the highest BMI, together with the fullname and the respective BMI. Dont forget they might have the same BMI.
  * 
  * Remember: BMI = mass/ height ^2 = mass / (height * height). (mass in kg and heigh in meter)
  * 
  * good luck
  */

  // my solutions

  var mark = {
	  fullname : 'Mark',
	  mass     : 89,
	  height   : 1.69,
	  calcBmi  : function (){
		  this.bmi = (this.mass / (this.height*this.height)) 
		  return this.bmi
	  }
  }

  var john = {
	  fullname: 'John',
	  mass: 92,
	  height: 1.78,
	  calcBmi : function (){
		  this.bmi = (this.mass / (this.height*this.height)) 
		return this.bmi
	}
  }
  
  if(john.calcBmi() > mark.calcBmi()){
	  console.log('Mark has higher BMI than john')
	} else {
		console.log('JOhn has higher BMI than Mark')
	}
	
	  console.log('john', john)
	  console.log('mark', mark)

// Jonas solution almost same with me, finally
// YAATTTAA