var log = console.log
/*********************************
 * FUNCTIONS
 */

 function calculateAge(birthYear) {
     return 2019 - birthYear
 }
//  calculateAge(1990) //straight executed

 var ageJohn = calculateAge(1990) //use variable to execute
 var ageMike = calculateAge(1948)
 var ageJane = calculateAge(1969)

 console.log('ageJohn', ageJohn, 'ageMike', ageMike, 'ageJane', ageJane)

 function yearsUntilRetirement(year, firstName) {
     var age = calculateAge(year);
     var retirement = 65 - age;

     if(retirement > 0) {
         console.log(firstName + ' retires in ' + retirement + ' years.')
     } else {
         console.log(firstName + ' is already retired')
     }
 }

 yearsUntilRetirement(1990, 'John')
 yearsUntilRetirement(1969, 'MIke')
 yearsUntilRetirement(1940, 'Jane')

 /************************************************
  * FUNCTIONS STATEMENT & FUNCTIONS EXPRESSION
  */

  //Function declaration
  function whatDoYouDo(job, firstName) {}


  // retrun keyword when we hit return keyword then we return whatever we define after it but what also happens is that the functions immediately finishes, so basically we immediately go out of the function ad return to the place where we actually called the function.

  // return keyword not only return the values but it also immediately finishes the function

  //Function expression
 var whatDoYouDo = function(job, firstName){
     switch(job) {
         case 'teacher':
            return firstName + ' teaches kids how to code'
         case 'driver':
             return firstName + ' drives a cab in lisbon'
         case 'designer':
             return firstName + ' designs beautiful websites'
         default :
            return firstName + ' does something else'
     }
 }

//  whatDoYouDo('teacher', 'john')
 log('whatDoYouDo', whatDoYouDo('teacher', 'john'))
 log('whatDoYouDo', whatDoYouDo('driver', 'mike'))
 log('whatDoYouDo', whatDoYouDo('retired', 'jane'))